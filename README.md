# DispTracker
=============

Code to track the displacement of a cantilever setup excited by a random signal captured using a high speed camera

# Usage
=======

python track.py --video path/to/video
