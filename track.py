#! usr/bin/env python
#! author: 4di

from imutils.video import count_frames
import argparse
import imutils
import cv2
import numpy as np
import time as ti
from mpi4py import MPI
from tools import *

# parallel stuff
# process 0 --> root
# rest of the processes --> workers
comm = MPI.COMM_WORLD
worldSize = comm.Get_size()
myRank = comm.Get_rank()
root = 0

# parallel execution: also works with one process, but then the communication overhead is unnecessary
if worldSize > 1:

	if myRank is root:
		# construct the argument parser and parse the arguments
		ap = argparse.ArgumentParser()
		Info = argParseWrapper(ap, worldSize)

	else:
		# initialize the values that will be broadcasted from the root
		Info = None

        # time the process
        if myRank is root:
                start = ti.time()

	# broadcast the user inputs from the root to the worker processes
	Info = comm.bcast(Info, root)

	# compute the process specific initial frame
	initialFrame = myRank * Info['FramesPerThread']

	# analyse the displacement in the frames local to each process
	localDisplacement, Info = extractDisplacement(initialFrame, Info)

	# wait for all the processes to complete the analysis
	comm.Barrier()

	# collect the length of every local displacement vector from each process
	localArraySize = np.array(comm.gather(len(localDisplacement), root))

	# gather the rectangular dimensions from each frame and sum it at the root
	if myRank is root:
		RectDim = np.zeros_like(Info['RectDim'])
	else:
		RectDim = None

	comm.Reduce([Info['RectDim'], MPI.INT], [RectDim, MPI.INT], op=MPI.SUM, root=root)

	# gather all the local displacements from the local process at the root
	if myRank is root:
		totalArraySize = sum(localArraySize)
		totalDisplacement = np.empty((totalArraySize, Info['NCols']), dtype=np.float64)
		localArraySize = localArraySize * Info['NCols']
	else:
		totalDisplacement = None

	comm.Gatherv(sendbuf=localDisplacement, recvbuf=(totalDisplacement, localArraySize), root=root)

	# post-process the analysed video feed
	if myRank is root:
		# this step is necessary to reshape the displacement matrix in the required order
		totalDisplacement = reshape(totalDisplacement, worldSize)

		# time the process
		print 'Done in', ti.time()-start, 's'

		# save the result
		saveResult(totalDisplacement, Info)

# serial execution: this implementation prevents unnecessary communication overhead in case the code is called with only
# one prcoess
else:
        # parse the input arguments and extract the necessary user provided information
        ap = argparse.ArgumentParser()
        Info = argParseWrapper(ap, worldSize)

        # time the process
        start = ti.time()

        # analyse the total displacement in the input video feed
        totalDisplacement = extractDisplacement(0, Info)

        # time the process
        print 'Done in', ti.time()-start, 's'

	# save the result
	saveResult(totalDisplacement, Info)

