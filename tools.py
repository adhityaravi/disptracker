#! usr/bin/env python
#! author: 4di

import cv2
import imutils
import numpy as np
import pandas as pd
import os.path
import matplotlib.pyplot as plt
import glob
import warnings

def extractDisplacement(initialFrame, Info, showFeed=False):
        """
        Extracts the displacement from the specified set of frames in the video (based on A.Rosenbrock's script)

        :param initialFrame: Starting frame number
        :param Info: Dictionary with user input information
        :param showFeed: Option to play the input video feed
        :return Displacement: information
        """

        # Open the video file
        video = cv2.VideoCapture(Info['VideoFile'])

        # instruct the cv2 reader to start from the specified initial frame
        video.set(1, initialFrame)

        # initialize the first frame to None as it will be defined later based on the starting frame of the video
        firstFrame = None

        # initialize frame counter and Displacement dictionary. The Displacement dictionary holds the x and y
        # coordinates of the moving object
        frameCounter = 0
        _Displacement = {'x': np.array([]), 'y': np.array([]), 'FrameCounter': np.array([])}

        # dimensions of the rectangle enclosing the point
        totWid = 0
        totHgt = 0

        # loop over the frames of the video and extract the displacement
        while frameCounter < Info['FramesPerThread']:

                # grab the current frame
                currFrame = video.read()
                currFrame = currFrame[1]

                # if the frame could not be grabbed, then we have reached the end
                # of the video
                if currFrame is None:
                        break

                # resize the frame, convert it to grayscale, and blur it
                currFrame = imutils.resize(currFrame, width=500)
                gray = cv2.cvtColor(currFrame, cv2.COLOR_BGR2GRAY)

                # if the first frame is None, initialize it
                if firstFrame is None:
                        frameSize = gray.shape
                        firstFrame = np.zeros(frameSize, dtype=np.uint8)

                # compute the absolute difference between the current frame and
                # first frame
                frameDelta = cv2.absdiff(firstFrame, gray)
                thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

                # dilate the thresholded image to fill in holes, then find contours
                # on thresholded image
                thresh = cv2.dilate(thresh, None, iterations=2)
                cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_SIMPLE)
                cnts = imutils.grab_contours(cnts)

                # loop over the contours
                for c in cnts:
                        # if the contour is too small, ignore it
                        if cv2.contourArea(c) < Info['MinArea']:
                                continue

                        # compute the bounding box for the contour, draw it on the frame,
                        # and update the text
                        (x, y, w, h) = cv2.boundingRect(c)
                        cv2.rectangle(currFrame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                        _Displacement['x'] = np.append(_Displacement['x'], ((x + w) / 2))
                        _Displacement['y'] = np.append(_Displacement['y'], ((y + h) / 2))
                        # compute the additive length of the rectangular contour
                        totWid = totWid + w
                        totHgt = totHgt + h

                # display the feed
                if showFeed is True:
                        cv2.imshow("Original Feed", currFrame)

                key = cv2.waitKey(1) & 0xFF
                # if the `q` key is pressed, break from the loop
                if key == ord("q"):
                        break

                _Displacement['FrameCounter'] = np.append(_Displacement['FrameCounter'], (initialFrame+frameCounter))

                frameCounter += 1

        # close any open windows
        video.release()
        cv2.destroyAllWindows()

        # dictionary to numpy array to facilitate gathering by the root
        Displacement = np.array(_Displacement.values()).T
        # compute the mean dimensions of the rectangular contour
        wid = totWid/len(Displacement)
        hgt = totHgt/len(Displacement)
        Info['RectDim'] = np.array([wid, hgt])

        return Displacement, Info

def argParseWrapper(aPObj, worldSize):
        """
        Wrapper for the argParse class to fetch user input infos

        :param aPObj: argParse object
        :param worldSize: Number of cores in case of parallel execution
        :return Info: Dictionary with user input information
        """

        aPObj.add_argument("-v", "--video", help="path to the video file")
        aPObj.add_argument("-a", "--min_area", type=int, default=500, help="minimum area size")
        aPObj.add_argument("-p", "--points", type=int, default=1, help="number of points to be tracked")
        aPObj.add_argument("-s", "--saveas", help="Filename with extension (.csv or .png) to save the result")
        aPObj.add_argument("-fps", "--framespersec", type=int, default=1, help="Frame rate of the input video feed")
        aPObj.add_argument("-dia", "--diameter", type=np.float64, help="Diameter of the tracked point")
        args = vars(aPObj.parse_args())

        # Assignment for ease of use and readability
        nPts = args["points"]

        # Calculate the number of columns in the final Displacement matrix
        nCols = (nPts*2) + 1 # 2 columns per point and 1 column for the frame counter

        # if the video argument is None, then throw an error
        if args.get("video", None) is None:
                raise ValueError('Video file name not available')

        # otherwise, we are reading from a video file
        else:
                videoFile = args["video"]
                if not os.path.isfile(videoFile):
                        raise ValueError('Enter a valid file name')

        # count the number of frames in the input video file
        totalFrames = 100#count_frames(videoFile)

        # compute the number of frames each thread has to analyse
        framesPerThread = totalFrames/worldSize

        Info = {'VideoFile': videoFile, 'MinArea': args["min_area"], 'TotalFrames': totalFrames,
                'FramesPerThread': framesPerThread, 'NPts': nPts, 'NCols': nCols, 'SaveAs': args["saveas"],
                'FPS': args["framespersec"], 'Diameter': args["diameter"]}

        return Info

def saveResult(dispMatrix, Info):
        """
        Function to plot or save the analysed displacement

        :param dispMatrix: Displacement of the moving point
        :param Info: Dictionary with user input information
        """

        # normalizing dispMatrix with the starting position of the tracked point
        for iCol in range(0, Info['NCols']-1):
                dispMatrix[:, iCol] = dispMatrix[:, iCol] - dispMatrix[0, iCol]

        # convert frame number to time
        if Info['FPS'] > 1:
                dispMatrix[:, -1] = (dispMatrix[:, -1]/Info['FPS'])
                xLabel = 'Time [s]'
        else:
                xLabel = 'Frames [-]'

        # scale the displacement with the give scaling factor
        if Info['Diameter'] is not None:
                scalingFactor = Info['Diameter']/min(Info['RectDim'])
                dispMatrix[:, 0:-2] = (dispMatrix[:, 0:-2] * scalingFactor)
                yLabel = 'Displacement [mm]'
        else:
                yLabel = 'Displacement [pixels]'

        # Max, Min, Crest factor and RMS computation
        # warning exception
        warnings.filterwarnings("error")

        yMax = '%.2f' % (max(dispMatrix[:, 0]))
        yMin = '%.2f'%(min(dispMatrix[:, 0]))
        yRMS = np.sqrt(np.mean(dispMatrix[:, 0]**2))
        try:
                yCF = '%.2f'%(float(yMax)/yRMS)
        except RuntimeWarning:
                yCF = 'Nan'
        yRMS = '%.2f'%(yRMS)

        xMax = '%.2f' % (max(dispMatrix[:, 1]))
        xMin = '%.2f'%(min(dispMatrix[:, 1]))
        xRMS = np.sqrt(np.mean(dispMatrix[:, 1] ** 2))
        try:
                xCF = '%.2f'%(float(xMax)/xRMS)
        except RuntimeWarning:
                xCF = 'Nan'
        xRMS = '%.2f'%(xRMS)

        # plot the result
        fig = plt.figure()
        plt.plot(dispMatrix[:, -1], dispMatrix[:, 0], 'b', linewidth=2, label='y Displacement')
        plt.plot(dispMatrix[:, -1], dispMatrix[:, 1], 'r', linewidth=2, label='x Displacement')
        plt.xlabel(xLabel, fontsize=18)
        plt.ylabel(yLabel, fontsize=18)
        text1 = 'yMax = ' + str(yMax) + ' yMin = ' + str(yMin) + ' yCrestFactor = ' + str(yCF) + ' yRMS = ' + str(yRMS)
        text2 = 'xMax = ' + str(xMax) + ' xMin = ' + str(xMin) + ' xCrestFactor = ' + str(xCF) + ' xRMS = ' + str(xRMS)
        plt.ylim(top=float(yMax)+1)
        plt.text(0, float(yMax)+0.7, text1, fontsize=16)
        plt.text(0, float(yMax)+0.5, text2, fontsize=16)
        plt.legend(fontsize=16)
        plt.show()

        # save the result
        if Info['SaveAs'] is not None:
                name, ext = os.path.splitext(Info['SaveAs'])
                directory = os.path.dirname(Info['VideoFile'])
                name = os.path.join(directory, name)

                # save as png
                nPrevFiles = len(glob.glob(name+'*.png'))
                nPrevFiles = nPrevFiles + len(glob.glob(name+'*.PNG'))
                if nPrevFiles > 0:
                        name = name + str(nPrevFiles)
                fig.savefig((name+'.png'), dpi=300, bbox_inches='tight')

                # save as csv
                CSV = pd.DataFrame(dispMatrix)
                CSV.columns = ['y1', 'x1', xLabel]
                nPrevFiles = len(glob.glob(name + '*.csv'))
                nPrevFiles = nPrevFiles + len(glob.glob(name + '*.CSV'))
                if nPrevFiles > 0:
                        name = name + str(nPrevFiles)
                CSV.to_csv(name+'.csv')

def reshape(dispMatrix, worldSize):
        """
        Reshape the displacement matrix after the root gathers the matrices from all the threads
        (quick and dirty implementation)

        :param dispMatrix: gathered, unordered displacement matrix
        :param worldSize: Number of cores in case of parallel execution
        :return ordDispMatrix: ordered displacement matrix
        """

        # unravel the unordered matrix
        [nRows, nCols] =  dispMatrix.shape
        dispMatrix = np.reshape(dispMatrix, nRows * nCols, order='C')

        # initialize empty ordered matrix
        ordDispMatrix = np.empty((nRows, nCols), dtype=np.float64)

        # size parameters for the new ordered matrix
        lenPerThread = len(dispMatrix) / worldSize
        datPerCol = lenPerThread / nCols

        # reshaping the matrix
        for iThread in range(0, worldSize):
                startIdx = iThread * lenPerThread
                endIdx = startIdx + lenPerThread

                fubar = dispMatrix[startIdx:endIdx]
                fubar = np.reshape(fubar, (datPerCol, nCols), order='F')

                startIdx = iThread * datPerCol
                endIdx = startIdx + datPerCol

                ordDispMatrix[startIdx:endIdx, :] = fubar

        return ordDispMatrix
